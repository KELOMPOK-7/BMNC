"""bmnc URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.11/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.conf.urls import url, include
    2. Add a URL to urlpatterns:  url(r'^blog/', include('blog.urls'))
"""
from django.conf.urls import url, include
from django.contrib import admin
from django.views.generic.base import RedirectView

import user_auth.urls as auth
import user_registration.urls as registration
import menu_berita.urls as berita
import menu_polling.urls as polling
import user_profile.urls as profile

urlpatterns = [
    url(r'^admin/', admin.site.urls),
    url(r'^$', RedirectView.as_view(url='/registration/', permanent='true'), name='index'),
    url(r'^auth/', include(auth, namespace='auth')),
    url(r'^registration/', include(registration, namespace='registration')),
    url(r'^berita/', include(berita, namespace='berita')),
    url(r'^polling/', include(polling, namespace='polling')),
    url(r'^profile/', include(profile, namespace='profile'))
]
