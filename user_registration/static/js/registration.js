function selectRole(role) {
  $("#role-dropdown").html(role);
  $("#role-input").empty();

  if (role === "Mahasiswa") {
    $("#role-input").append(
      "<label>Status Kemahasiswaan</label>" +
      "<input id='field_status_mahasiswa' type='text' class='form-control' placeholder='Ex: Active / Not Active' required> </div>"
    );
  }
  else if (role === "Staff" || role === "Dosen") {
    $.ajax({
      method : 'GET',
      url : "get-universitas",
      success : function(response) {
        var data = response["data"];
        var html = "<label>ID Universitas</label>" +
                   "<br>" +
                   '<div class="btn-group">' +
                   '<button id="universitas-dropdown" class="btn dropdown-toggle btn-warning" data-toggle="dropdown">' + data[0]['id'] + '</button>' +
                   '<div class="dropdown-menu scrollable-menu">';
        for (i = 0; i < data.length; i++) {
          var id = data[i]['id'];
          var nama = data[i]['nama'];
          html += '<a class="dropdown-item btn" onClick="selectUniversitas(' + id + ')">(' + id + ') ' + nama + '</a>';

          if (i != (data.length - 1)) {
            html += '<div class="dropdown-divider"></div>';
          }
        }
        html += '</div>' +
                '</div>';
        $('#role-input').append(html);
      },
      error : function(error) {
        alert("error");
      }
    });
  }
}

function selectUniversitas(id) {
  $("#universitas-dropdown").html(id);
}

function register() {
  if (!isInputEmpty()) {
    $(".registration_message").empty();
    $("html, body").animate({ scrollTop: 0 }, "slow");
    $(".registration_message").append(
      '<div class="alert alert-primary text-center font-weight-bold" role="alert">' +
      "Proses Registrasi Anda Sedang Diproses, Mohon Untuk Menunggu" +
      "</div>"
    );

    var role = $('#role-dropdown').html();

    if (role === 'Mahasiswa') {
      var status_mahasiswa = $('#field_status_mahasiswa').val();
      var id_universitas = 1;
    }
    else if (role === "Staff" || role === "Dosen") {
      var status_mahasiswa = '';
      var id_universitas = $('#universitas-dropdown').html();
    }

    $.ajax({
      method : 'POST',
      url : 'register',
      data : {
        role : role,
        username : $('#field_username_register').val(),
        password : $('#field_password_register').val(),
        nomor_identitas : $('#field_nomor_identitas').val(),
        nama : $('#field_nama').val(),
        tempat_lahir : $('#field_tempat_lahir').val(),
        tanggal_lahir : $('#field_tanggal_lahir').val(),
        email : $('#field_email').val(),
        nomor_hp : $('#field_nomor_hp').val(),
        status : status_mahasiswa,
        id_univ : id_universitas
      },
      dataType : 'json',
      success : function(response) {
        if (response["valid"]) {
          location.reload(true);
        }
        else {
          $(".registration_message").empty();
          var html = '<div class="alert alert-danger text-center font-weight-bold" role="alert">';
          for (i = 0; i < response["messages"].length; i++) {
            html += response["messages"][i] + "<br>";
          }
          html += "</div>";
          $(".registration_message").append(html);
        }
      },
      error : function(error) {
        alert("error!")
      }
    });
  }
}

function isInputEmpty() {
  if ($('#field_username_register').val().length == 0) {
    alert("Input Username tidak boleh kosong!");
    return true;
  }
  else if ($('#field_password_register').val().length == 0) {
    alert("Input password tidak boleh kosong!");
    return true;
  }
  else if ($('#field_nomor_identitas').val().length == 0) {
    alert("Input Nomor Identitas tidak boleh kosong!");
    return true;
  }
  else if ($('#field_nama').val().length == 0) {
    alert("Input Nama tidak boleh kosong!");
    return true;
  }
  else if ($('#field_tempat_lahir').val().length == 0) {
    alert("Input Tempat Lahir tidak boleh kosong!");
    return true;
  }
  else if ($('#field_tanggal_lahir').val().length == 0) {
    alert("Input Tanggal Lahir tidak boleh kosong!");
    return true;
  }
  else if ($('#field_email').val().length == 0) {
    alert("Input Email tidak boleh kosong!");
    return true;
  }
  else if ($('#role-dropdown').html() === 'Mahasiswa' && $('#field_status_mahasiswa').val().length == 0) {
    alert("Input Status Mahasiswa tidak boleh kosong!");
    return true;
  }

  return false;
}
