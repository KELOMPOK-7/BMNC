# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from django.shortcuts import render
from django.http import HttpResponseRedirect, JsonResponse
from django.urls import reverse
from django.views.decorators.csrf import csrf_exempt
from django.db import connection

response = {}
# Create your views here.
def index(request):
    if "username" not in request.session.keys():
        response['header_login'] = True
        html = 'registration.html'

        return render(request, html, response)
    else:
        #WARNING TEMP JUST TO SHOW LOGOUT, CHANGE THIS
        response['header_login'] = False
        html = 'registration.html'
        return render(request, html, response)
        #return HttpResponseRedirect(reverse('registration:index')) #WARNING change this to main page

def get_universitas(request):
    if request.method == 'GET':
        lst = []

        with connection.cursor() as cursor:
            cursor.execute("SELECT id, nama FROM UNIVERSITAS")
            for objek in cursor.fetchall():
                lst.append({'id' : objek[0], 'nama' : objek[1]})


        data = {
        'data' : lst
        }
        return JsonResponse(data)
    else:
        return HttpResponseRedirect(reverse('registration:index')) #WARNING change this to main page

@csrf_exempt
def register(request):
    if request.method == 'POST':
        messages = validasi(request.POST["username"], request.POST["nomor_identitas"], request.POST["password"], request.POST["role"])
        if messages["valid"]:
            insertData(request.POST)
            request.session["username"] = request.POST["username"] #automatic login

        return JsonResponse(messages)
    else:
        return HttpResponseRedirect(reverse("registration:index"))

def validasi(username, nomor_identitas, password, role):
    messages = {"messages" : []}
    checkUsername(messages, username)
    checkNomorIdentitas(messages, nomor_identitas, role)
    checkPassword(messages, password)

    if len(messages["messages"]) == 0:
        messages["valid"] = True
    else:
        messages["valid"] = False

    return messages

def insertData(data):
    role = data["role"]
    username = data["username"]
    password = data["password"]
    nomor_identitas = data["nomor_identitas"]
    nama = data["nama"]
    tempat_lahir = data["tempat_lahir"]
    tanggal_lahir = data["tanggal_lahir"]
    email = data["email"]
    nomor_hp = data["nomor_hp"]
    status = data["status"]
    id_univ = data["id_univ"]

    with connection.cursor() as cursor:
        cursor.execute("SELECT COUNT(*) FROM NARASUMBER")
        id = cursor.fetchone()[0]
        cursor.execute("INSERT INTO NARASUMBER VALUES (%s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s)", [id, nama, email, tempat_lahir, tanggal_lahir, nomor_hp, 0, 0,int(id_univ),0, username, password])

        if role == "Mahasiswa":
            cursor.execute("INSERT INTO MAHASISWA VALUES (%s, %s, %s)", [id, nomor_identitas, status])
        elif role == "Staff":
            cursor.execute("INSERT INTO STAF VALUES (%s, %s, %s)", [id, nomor_identitas, "staff"])
        elif role == "Dosen":
            cursor.execute("INSERT INTO DOSEN VALUES (%s, %s, %s)", [id, nomor_identitas, "Dosen"])

def checkUsername(messages, username):
    valid = False
    with connection.cursor() as cursor:
        cursor.execute("SELECT username FROM NARASUMBER WHERE username = %s", [username])
        valid = cursor.fetchone() == None

    if not valid:
        messages["messages"].append("Username Sudah Terdaftar di Database!")

def checkNomorIdentitas(messages, nomor_identitas, role):
    valid = False

    #CHECK nomor identitas FROM Database
    if role == "Mahasiswa":
        with connection.cursor() as cursor:
            cursor.execute("SELECT npm FROM MAHASISWA WHERE npm = %s", [nomor_identitas])
            valid = cursor.fetchone() == None
    elif role == "Staff":
        with connection.cursor() as cursor:
            cursor.execute("SELECT nik_staf FROM staf WHERE nik_staf = %s", [nomor_identitas])
            valid = cursor.fetchone() == None
    elif role == "Dosen":
        with connection.cursor() as cursor:
            cursor.execute("SELECT nik_dosen FROM DOSEN WHERE nik_dosen = %s", [nomor_identitas])
            valid = cursor.fetchone() == None

    if not valid:
        messages["messages"].append("Nomor Identitas Sudah Terdaftar di Database!")

def checkPassword(messages, password):
    if len(password) < 8:
        messages["messages"].append("Banyak Password Minimal 8 Karakter!")

    hasAngka = False
    hasHuruf = False
    for i in password:
        if i.isdigit():
            hasAngka = True
            break
    for i in password:
        if not i.isdigit():
            hasHuruf = True
            break
    if not hasAngka:
        messages["messages"].append("Password Harus Mengandung Angka!")
    if not hasHuruf:
        messages["messages"].append("Password Harus Mengandung Huruf!")
