from django.conf.urls import url
from .views import index, get_universitas, register

urlpatterns = [
    url(r'^$', index, name='index'),
    url(r'^get-universitas', get_universitas, name='get_universitas'),
    url(r'^register', register, name='register')
]
