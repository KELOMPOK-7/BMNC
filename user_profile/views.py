from django.shortcuts import render
from django.http import HttpResponseRedirect, JsonResponse
from django.urls import reverse
from django.views.decorators.csrf import csrf_exempt

from django.db import connection
from collections import namedtuple

# Create your views here.
response = {}
cursor = connection.cursor()
# Create your views here.
def index(request):
	if "username" in request.session.keys():
		response['header_login'] = False
		html = 'profile.html'
		
		username =  request.session["username"]
		
		cursor.execute("SELECT * FROM narasumber where username=%s", [username])
		result = namedtuplefetchall(cursor)[0]
		response["result"] = result
		
		cursor.execute("SELECT * FROM universitas where id=%s", [result.id_universitas])
		universitas = namedtuplefetchall(cursor)[0]
		response["universitas"] = universitas
		
		cursor.execute("SELECT * FROM mahasiswa where id_narasumber=%s", [result.id])
		mahasiswa = namedtuplefetchall(cursor)
		
		cursor.execute("SELECT * FROM staf where id_narasumber=%s", [result.id])
		staf = namedtuplefetchall(cursor)
		
		cursor.execute("SELECT * FROM dosen where id_narasumber=%s", [result.id])
		dosen = namedtuplefetchall(cursor)
		
		if (len(mahasiswa) > 0):
			response["mahasiswa"] = mahasiswa[0]
		elif (len(staf) > 0):
			response["staf"] = staf[0]
		elif (len(dosen) > 0):
			response["dosen"] = dosen[0]
		
		
		cursor.execute("SELECT * FROM berita where url in (SELECT url_berita from narasumber_berita where id_narasumber = %s)", [result.id])
		berita = namedtuplefetchall(cursor)
		response["dosen"] = dosen
		
		return render(request, html, response)
	else:
		return HttpResponseRedirect(reverse('registration:index'))

def namedtuplefetchall(cursor):
    "Return all rows from a cursor as a namedtuple"
    desc = cursor.description
    nt_result = namedtuple('Result', [col[0] for col in desc])
    return [nt_result(*row) for row in cursor.fetchall()]