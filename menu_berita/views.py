from django.shortcuts import render
from django.http import HttpResponseRedirect, JsonResponse
from django.urls import reverse
from django.views.decorators.csrf import csrf_exempt
from django.db import connection
from datetime import datetime
from collections import namedtuple

# Create your views here.
response = {}
cursor = connection.cursor()
# Create your views here.
def index(request):
	if "username" in request.session.keys():
		response['header_login'] = False
		html = 'menuberita.html'
		return render(request, html, response)
	else:
		return HttpResponseRedirect(reverse('registration:index'))

def createnews(request):
	if "username" in request.session.keys():
		response['header_login'] = False
		html = 'addnews.html'

		return render(request, html, response)
	else:
		return HttpResponseRedirect(reverse('registration:index'))

def namedtuplefetchall(cursor):
    "Return all rows from a cursor as a namedtuple"
    desc = cursor.description
    nt_result = namedtuple('Result', [col[0] for col in desc])
    return [nt_result(*row) for row in cursor.fetchall()]

def addnews(request):
	if "username" in request.session.keys():
		response['header_login'] = False
		html = 'confirm.html'

		now = datetime.now()
		now = now.strftime("%Y-%m-%d %H:%M:%S")

		username = request.session["username"]
		cursor.execute("SELECT id_universitas FROM NARASUMBER N WHERE N.username=%s", [username])
		res = namedtuplefetchall(cursor)
		id_univ = res[0].id_universitas

		judul = str(request.POST.get("title"))
		id_berita = str(request.POST.get("id-berita"))
		topik = str(request.POST.get("topic"))
		kata = int(str(request.POST.get("words")))
		konten = str(request.POST.get("content"))
		url = "https://bmnc.com/news/" + str(id_berita)


		cursor.execute("INSERT INTO BERITA VALUES(%s, %s, %s, %s, %s, %s, 0, %s);",
			[url,judul,topik,now,now,kata,id_univ])

		return render(request, html, response)
	else:
		return HttpResponseRedirect(reverse('registration:index'))
