from django.conf.urls import url
from .views import index, createnews, addnews

urlpatterns = [
    url(r'^$', index, name='index'),
    url(r'^createnews', createnews, name='createnews'),
    url(r'^addnews', addnews, name='addnews')
]
