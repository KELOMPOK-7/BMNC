from django.shortcuts import render
from django.http import HttpResponseRedirect
from django.urls import reverse
from django.contrib import messages
from django.db import connection

# Create your views here.
def login(request):
    if request.method == 'POST':
        username = request.POST["field_username"]
        password = request.POST["field_password"]

        if valid(username, password):
            request.session['username'] = username
        else:
            messages.warning(request, "Username atau Password Salah! Belum Punya Account? Klik Register")
            return HttpResponseRedirect(request.META['HTTP_REFERER'])

    return HttpResponseRedirect(reverse('registration:index')) #WARNING change this to main page

def logout(request):
    request.session.flush()
    return HttpResponseRedirect(reverse('registration:index')) #WARNING change this to main page

#check from database
def valid(username, password):
    result = ""
    with connection.cursor() as cursor:
        cursor.execute("SELECT username, password FROM NARASUMBER WHERE username = %s AND password = %s", [username, password])
        result = cursor.fetchone()
        print("result", result)
    return result != None
