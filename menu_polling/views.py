from django.shortcuts import render
from django.http import HttpResponseRedirect, JsonResponse
from django.urls import reverse
from django.views.decorators.csrf import csrf_exempt
from django.db import connection
from collections import namedtuple

# Create your views here.
response = {}
cursor = connection.cursor()
# Create your views here.
def index(request):
	if "username" in request.session.keys():
		response['header_login'] = False

		cursor.execute("SELECT * FROM polling where id in  (SELECT id from polling_berita)")
		polling_berita = namedtuplefetchall(cursor)
		addUrl(polling_berita)

		cursor.execute("SELECT * FROM polling where id in  (SELECT id from polling_biasa)")
		polling_biasa = namedtuplefetchall(cursor)
		addDeskripsi(polling_biasa)

		html = 'menupoll.html'
		return render(request, html, response)
	else:
		return HttpResponseRedirect(reverse('registration:index'))

def createNewsPoll(request):
	if "username" in request.session.keys():
		response['header_login'] = False
		html = 'newspoll.html'
		return render(request, html, response)
	else:
		return HttpResponseRedirect(reverse('registration:index'))

def createOwnPoll(request):
	if "username" in request.session.keys():
		response['header_login'] = False
		html = 'ownpoll.html'
		return render(request, html, response)
	else:
		return HttpResponseRedirect(reverse('registration:index'))

def addNewsPoll(request):
	if "username" in request.session.keys():
		response['header_login'] = False
		html = 'confirmnewspoll.html'

		geturl = request.POST.get("idurl")
		starttime = str(request.POST.get("starttime"))
		endtime = str(request.POST.get("endtime"))
		question = str(request.POST.get("question"))
		ans1 = str(request.POST.get("answer1"))
		ans2 = str(request.POST.get("answer2"))

		url = "https://bmnc.com/news/" + str(geturl)
		print(starttime)
		starttime = changeFormatTime(starttime)
		endtime = changeFormatTime(endtime)

		cursor.execute("SELECT count(*) as jumlah FROM POLLING;")
		res = namedtuplefetchall(cursor)
		print(type(res), res[0].jumlah)
		id_polling = int(res[0].jumlah) + 1

		cursor.execute("INSERT INTO POLLING VALUES(%s, %s, %s, 0);", [id_polling, starttime, endtime])
		cursor.execute("INSERT INTO POLLING_BERITA VALUES(%s, %s);", [id_polling, url])
		cursor.execute("INSERT INTO RESPON VALUES(%s, %s, 0);", [id_polling, ans1])
		cursor.execute("INSERT INTO RESPON VALUES(%s, %s, 0);", [id_polling, ans2])

		return render(request, html, response)
	else:
		return HttpResponseRedirect(reverse('registration:index'))

def addOwnPoll(request):
	if "username" in request.session.keys():
		response['header_login'] = False
		html = 'confirmownpoll.html'

		desc = request.POST.get("desc")
		starttime = request.POST.get("starttime")
		endtime = request.POST.get("endtime")
		question = request.POST.get("question")
		ans1 = request.POST.get("answer1")
		ans2 = request.POST.get("answer2")

		starttime = changeFormatTime(starttime)
		endtime = changeFormatTime(endtime)

		cursor.execute("SELECT count(*) as jumlah FROM POLLING;")
		res = namedtuplefetchall(cursor)
		print(type(res), res[0].jumlah)
		id_polling = int(res[0].jumlah) + 1

		cursor.execute("INSERT INTO POLLING VALUES(%s, %s, %s, 0);", [id_polling, starttime, endtime])
		cursor.execute("INSERT INTO POLLING_BIASA VALUES(%s, %s, 0);", [id_polling, desc])
		cursor.execute("INSERT INTO RESPON VALUES(%s, %s, 0);", [id_polling, ans1])
		cursor.execute("INSERT INTO RESPON VALUES(%s, %s, 0);", [id_polling, ans2])

		return render(request, html, response)
	else:
		return HttpResponseRedirect(reverse('registration:index'))

def changeFormatTime(time):
	splittedTime = time.split(" ")
	resultDate = time[0] # 12/01/2018 3:30 AM
	resultHour = time[1]
	timeHour = time[2]

	print(resultDate, resultHour, timeHour)

	if len(resultHour) == 7:
		resultHour = '0' + resultHour

	if timeHour == 'PM':
		splittedHour = resultHour.split(":")
		convertHour = int(splittedHour[0]) + 12
		resultHour = convertHour + ":" + splittedHour[1] + ":00"
	elif timeHour == 'AM':
		resultHour = "0" + resultHour + ":00"

	results = resultDate + " " + resultHour
	return results

def namedtuplefetchall(cursor):
    "Return all rows from a cursor as a namedtuple"
    desc = cursor.description
    nt_result = namedtuple('Result', [col[0] for col in desc])
    return [nt_result(*row) for row in cursor.fetchall()]

def addUrl(polling_berita):
	tambahan_berita = []
	for poll in polling_berita:
		cursor.execute("SELECT url_berita FROM polling_berita where id_polling = %s", [poll.id])
		tambahan_berita.append(namedtuplefetchall(cursor)[0])
	response["polling_berita"] = [polling_berita, tambahan_berita]


def addDeskripsi(polling_biasa):
	tambahan_biasa = []
	for poll in polling_biasa:
		cursor.execute("SELECT url, deskripsi FROM polling_biasa where id_polling = %s", [poll.id])
		tambahan_biasa.append(namedtuplefetchall(cursor)[0])
	response["polling_biasa"] = [polling_biasa, tambahan_biasa]
