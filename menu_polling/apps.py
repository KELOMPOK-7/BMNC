from django.apps import AppConfig


class MenuPollingConfig(AppConfig):
    name = 'menu_polling'
