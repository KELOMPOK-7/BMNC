from django.conf.urls import url
from .views import index, createNewsPoll, createOwnPoll, addOwnPoll, addNewsPoll

urlpatterns = [
    url(r'^$', index, name='index'),
    url(r'^createNewsPoll', createNewsPoll, name='createNewsPoll'),
    url(r'^createOwnPoll', createOwnPoll, name='createOwnPoll'),
    url(r'^confirmNewsPoll', addNewsPoll, name='addNewsPoll'),
    url(r'^confirmOwnPoll', addOwnPoll, name='addOwnPoll')
]
